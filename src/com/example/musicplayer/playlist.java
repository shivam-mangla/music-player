package com.example.musicplayer;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class playlist extends Activity {
	ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.playlistview);
		ArrayList<HashMap<String, String>> songsListData = new ArrayList<HashMap<String, String>>();
		manager mngr = new manager();
		this.songsList = mngr.getPlayList();
		for (int i = 0; i < songsList.size(); i++) {
			HashMap<String, String> song = songsList.get(i);
			songsListData.add(song);
		}
		ListView lv = (ListView) findViewById(R.id.listView1);
		ListAdapter adapter = new SimpleAdapter(this, songsListData,
				R.layout.playlist, new String[] { "songTitle" },
				new int[] { R.id.listView1 });
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				int songIndex = position;
				Intent in = new Intent(getApplicationContext(),
						MainActivity.class);
				in.putExtra("songIndex", songIndex);
				setResult(100, in);
				finish();
			}

		});
	}
}