package com.example.musicplayer;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class manager {
	ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
	final String Media_Path = ("/sdcard/");

	public ArrayList<HashMap<String, String>> getPlayList() {
		File home = new File(Media_Path);
		if (home.listFiles(new FileExtensionFilter()).length > 0) {
			for (File file : home.listFiles(new FileExtensionFilter())) {
				HashMap<String, String> song = new HashMap<String, String>();
				song.put("songTitle", file.getName());
				song.put("songPath", file.getPath());
				songsList.add(song);
			}
		}
		return songsList;
	}
}
