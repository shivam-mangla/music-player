package com.example.musicplayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.R;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity {
	ImageButton btnplay, btnnext, btnprevious;
	Button playlist;
	TextView text;
	MediaPlayer mp = new MediaPlayer();
	ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
	int currentSongIndex = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btnplay = (ImageButton) findViewById(R.id.playpause);
		btnnext = (ImageButton) findViewById(R.id.next);
		btnprevious = (ImageButton) findViewById(R.id.previous);
		playlist = (Button) findViewById(R.id.playlist);
		text = (TextView) findViewById(R.id.textView1);

		// Play-Pause
		btnplay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (mp.isPlaying()) {
					if (mp != null) {
						mp.pause();
						btnplay.setImageResource(R.drawable.ic_media_play);
					}
				} else {
					if (mp != null) {
						mp.start();
						btnplay.setImageResource(R.drawable.ic_media_pause);
					}
				}
			}
		});
		// Next-Previous
		btnnext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (currentSongIndex < (songsList.size() - 1)) {
					playSong(currentSongIndex + 1);
					currentSongIndex = currentSongIndex + 1;
				} else {
					playSong(0);
					currentSongIndex = 0;
				}
			}
		});
		btnprevious.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (currentSongIndex > 0) {
					playSong(currentSongIndex - 1);
					currentSongIndex = currentSongIndex - 1;
				} else {
					playSong(songsList.size() - 1);
					currentSongIndex = songsList.size() - 1;
				}
			}
		});
		playlist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this, playlist.class);
				startActivityForResult(i, 100);
			}
		});
	}

	public void playSong(int songIndex) {
		// TODO Auto-generated method stub
		try {
			mp.reset();
			mp.setDataSource(songsList.get(songIndex).get("songPath"));
			mp.prepare();
			mp.start();
			// Displaying Song title
			String songTitle = songsList.get(songIndex).get("songTitle");
			text.setText(songTitle);

			// Changing Button Image to pause image
			btnplay.setImageResource(R.drawable.ic_media_pause);

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 100) {
			currentSongIndex = data.getExtras().getInt("songIndex");
			// play selected song
			playSong(currentSongIndex);
		}

	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		if (currentSongIndex < (songsList.size() - 1)) {
			playSong(currentSongIndex + 1);
			currentSongIndex = currentSongIndex + 1;
		} else {
			// play first song
			playSong(0);
			currentSongIndex = 0;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mp.release();
	}

}