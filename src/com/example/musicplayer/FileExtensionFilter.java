package com.example.musicplayer;

import java.io.File;
import java.io.FilenameFilter;

public class FileExtensionFilter implements FilenameFilter {

	@Override
	public boolean accept(File arg0, String arg1) {
		// TODO Auto-generated method stub
		return (arg1.endsWith(".mp3") || arg1.endsWith(".MP3"));
	}
}